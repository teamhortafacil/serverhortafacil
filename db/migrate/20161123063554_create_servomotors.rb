class CreateServomotors < ActiveRecord::Migration[5.0]
  def change
    create_table :servomotors do |t|
      t.boolean :requester
      t.boolean :active
      t.datetime :last_run

      t.timestamps
    end
  end
end
