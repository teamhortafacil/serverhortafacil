class ServomotorController < ApplicationController
    def hello
        render html: "Hello!"
    end

    def create
        @servomotor = Servomotor.new(
            :requester => false,
            :active => false,
            :last_run => DateTime.now)
        @servomotor.save
        render html: "Created servomotor %{id}" % { :id => @servomotor.id }
    end

    def activate
        @servomotor = Servomotor.find(params[:id])
        if !@servomotor.active
            @servomotor.update_attributes(:active => true)
            render html: "servo-motor activated."
        else
            render html: "servo-motor already running."
        end
    end

    def deactivate
        @servomotor = Servomotor.find(params[:id])
        if @servomotor.active
            @servomotor.update_attributes(:active => false, :last_run => DateTime.now)
            render html: "servo-motor deactivated."
        else
            render html: "servo-motor not running."
        end
    end

    def get_last
        @servomotor = Servomotor.find(params[:id])
        render json: @servomotor.last_run
    end
end
