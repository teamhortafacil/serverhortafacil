Rails.application.routes.draw do
  devise_for :users
  get 'users/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'hello', to: 'servomotor#hello'
  get 'servomotor/create', to: 'servomotor#create'
  get 'servomotor/activate/(:id)', to: 'servomotor#activate'
  get 'servomotor/deactivate/(:id)', to: 'servomotor#deactivate'
  get 'servomotor/last/(:id)', to: 'servomotor#get_last'
end
